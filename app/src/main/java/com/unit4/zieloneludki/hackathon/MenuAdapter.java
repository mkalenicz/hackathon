package com.unit4.zieloneludki.hackathon;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;


public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.MenuViewHolder> {

    private final ArrayList<HashMap<String, String>> menuList;

    public MenuAdapter(ArrayList<HashMap<String, String>> list) {
        menuList = list;
    }


    @Override
    public MenuViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_menu, parent, false);
        return new MenuViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MenuViewHolder holder, int position) {
        if (position >= getItemCount())
            return;

        HashMap<String, String> menuModel = menuList.get(position);


        holder.ingredientsInfo.setText(menuModel.get("ingredients"));
        String imgUrl = menuModel.get("thumbnail");
        String wholeTitle = menuModel.get("title");
        String title = wholeTitle.toString()
                .replace("\n", "")
                .trim();//remove the commas
        holder.menuTitle.setText(title);
        if (!imgUrl.isEmpty()) {
            Picasso.with(MainActivity.getContext())
                    .load(imgUrl)
                    .placeholder(R.drawable.ic_local_dining_black_24dp)
                    .error(R.drawable.ic_local_dining_black_24dp)
                    .into(holder.menuImg);
        }
//

    }

    @Override
    public int getItemCount() {
        return menuList.size();
    }

    class MenuViewHolder extends RecyclerView.ViewHolder {

        final ImageView menuImg;
        final TextView menuTitle;
        final TextView ingredientsInfo;

        MenuViewHolder(View listItem) {
            super(listItem);
            menuImg = listItem.findViewById(R.id.menu_img);
            menuTitle = listItem.findViewById(R.id.menu_title);
            ingredientsInfo = listItem.findViewById(R.id.ingredients_info);
        }
    }

}

package com.unit4.zieloneludki.hackathon;



import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


public class SimpleFragmentPagerAdapter extends FragmentPagerAdapter {

    private String recipes = MainActivity.getContext().getString(R.string.recipes_tab_title);
    private String yourProducts = MainActivity.getContext().getString(R.string.your_products_tab_title);
    private String tabTitles[] = new String[]{recipes, yourProducts};

    public SimpleFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new RecipesFragment();
        } else {
            return new ProductsFragment();
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }
}

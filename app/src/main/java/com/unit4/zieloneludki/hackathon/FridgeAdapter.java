package com.unit4.zieloneludki.hackathon;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

public class FridgeAdapter extends RecyclerView.Adapter<FridgeAdapter.FridgeViewHolder> {

    private final ArrayList<HashMap<String, String>> fridgeList;

    public FridgeAdapter(ArrayList<HashMap<String, String>> list) {
        fridgeList = list;
    }


    @Override
    public FridgeAdapter.FridgeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product, parent, false);
        return new FridgeAdapter.FridgeViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FridgeAdapter.FridgeViewHolder holder, int position) {
        if (position >= getItemCount())
            return;

        HashMap<String, String> fridgeModel = fridgeList.get(position);
        holder.productName.setText((fridgeModel.get("attributeName")));
        holder.productDaysLeft.setText(fridgeModel.get("attributeDaysLeft"));
        holder.productAmount.setText(fridgeModel.get("attributeAmount"));
//        String attributeKind = menuModel.get("attributeKind");

    }

    @Override
    public int getItemCount() {
        return fridgeList.size();
    }

    class FridgeViewHolder extends RecyclerView.ViewHolder {

        //        final ImageView menuImg;
        final TextView productName;
        final TextView productDaysLeft;
        final TextView productAmount;

        FridgeViewHolder(View listItem) {
            super(listItem);
            productName = listItem.findViewById(R.id.product_item_name);
            productAmount = listItem.findViewById(R.id.product_amount);
            productDaysLeft = listItem.findViewById(R.id.product_days_left);
        }
    }

}


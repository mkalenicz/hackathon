package com.unit4.zieloneludki.hackathon;


import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 */
public class RecipesFragment extends Fragment implements LoaderManager.LoaderCallbacks<ArrayList<HashMap<String, String>>> {

    String sampleFridge ="tomatoes, eggs, pasta, garlic";
    String givenData = " ";
    LoaderManager loaderManager;
    RecyclerView recyclerView;

    public RecipesFragment() {
        // Required empty public constructor

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_recipes, container, false);
        loaderManager = getLoaderManager();
        loaderManager.initLoader(0, null, this);

       recyclerView = view.findViewById(R.id.recipes_recycler_view);
        recyclerView.setHasFixedSize(true);
//        // use a linear layout manager
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.getContext());
        recyclerView.setLayoutManager(layoutManager);

//        // define an adapter
        return view;

    }

    @Override
    public Loader<ArrayList<HashMap<String, String>>> onCreateLoader(int id, Bundle args) {
        return new LoaderMenu(this.getContext(), sampleFridge);
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<HashMap<String, String>>> loader, ArrayList<HashMap<String, String>> datae) {
    if(datae!=null){
//        for(HashMap<String, String> map : data){

            RecyclerView.Adapter menuAdapter = new MenuAdapter(datae);
            recyclerView.setAdapter(menuAdapter);
    }
//    Log.i("test", weHave);
//        Toast.makeText(this.getContext(), weHave, Toast.LENGTH_LONG).show();

    }

    @Override
    public void onLoaderReset(Loader<ArrayList<HashMap<String, String>>> loader) {

    }
}

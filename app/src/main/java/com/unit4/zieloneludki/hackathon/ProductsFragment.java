package com.unit4.zieloneludki.hackathon;





import android.Manifest;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;

import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;


public class ProductsFragment extends Fragment implements LoaderManager.LoaderCallbacks<ArrayList<HashMap<String, String>>> {
    public static final String API_URL = "http://";
    static final int REQUEST_IMAGE_CAPTURE = 1;

    ArrayList<HashMap<String, String>> deviceList;
    LoaderManager loaderManager;
    RecyclerView recyclerView;
//    View view;
    public ProductsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_products, container, false);
        loaderManager = getLoaderManager();
        ButterKnife.bind(this, view);
        // Initialize the loader. Pass in the int ID constant defined above and pass in null for
        // the bundle. Pass in this activity for the LoaderCallbacks parameter (which is valid
        // because this activity implements the LoaderCallbacks interface).
        loaderManager.initLoader(1, null, this);

        recyclerView = (RecyclerView) view.findViewById(R.id.products_recycler_view);
        recyclerView.setHasFixedSize(true);
//        // use a linear layout manager
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        return view;
    }

    @Override
    public android.support.v4.content.Loader<ArrayList<HashMap<String, String>>> onCreateLoader(int id, Bundle args) {
        return new LoaderFridge(MainActivity.getContext(), API_URL);
    }

    @Override
    public void onLoadFinished(android.support.v4.content.Loader<ArrayList<HashMap<String, String>>> loader, ArrayList<HashMap<String, String>> data) {
       if (data == null) {
            return;
        }
        deviceList = data;

        View loadingIndicator = getView().findViewById(R.id.loading_indicator);
        loadingIndicator.setVisibility(View.GONE);

        RecyclerView.Adapter fridgeAdapter = new FridgeAdapter(data);
        recyclerView.setAdapter(fridgeAdapter);
        Log.i("Productfragment", "onloadfinished" + deviceList.toString());
    }

    @Override
    public void onLoaderReset(android.support.v4.content.Loader<ArrayList<HashMap<String, String>>> loader) {
        loader.reset();
    }

    @OnClick(R.id.fabPhoto)
    public void onFabButtonPressed(){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (ContextCompat.checkSelfPermission(this.getContext(), Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED){
            getPermission();} else {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);}
    }

    public void getPermission() {
        ActivityCompat.requestPermissions(this.getActivity(), new String[] {Manifest.permission.CAMERA}, REQUEST_IMAGE_CAPTURE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            //mImageView.setImageBitmap(imageBitmap);
            if (imageBitmap!=null){
                Toast.makeText(this.getContext(), "photo was captured", Toast.LENGTH_LONG).show();
            }

            FragmentTransaction ft = getFragmentManager().beginTransaction();
            Fragment prev = getFragmentManager().findFragmentByTag("dialog");
            if (prev != null) {
                ft.remove(prev);
            }
            ft.addToBackStack(null);

            DialogFragment newFragment = SendPhotoDialog.newInstance(imageBitmap);
            newFragment.show(ft, "dialog");

        }
    }
//    @Override
//    public Loader<ArrayList<HashMap<String, String>>> onCreateLoader(int id, Bundle args) {
//        return new LoaderFridge(MainActivity.getContext(), API_URL);
//    }
//
//    @Override
//    public void onLoadFinished(Loader<ArrayList<HashMap<String, String>>> loader, ArrayList<HashMap<String, String>> data) {
//        if (data == null) {
//            return;
//        }
//        deviceList = data;
//
//        View loadingIndicator = view.findViewById(R.id.loading_indicator);
//        loadingIndicator.setVisibility(View.GONE);
//    }
//
//    @Override
//    public void onLoaderReset(Loader<ArrayList<HashMap<String, String>>> loader) {
//        loader.reset();
//    }
}

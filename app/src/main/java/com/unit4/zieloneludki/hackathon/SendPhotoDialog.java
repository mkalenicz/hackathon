package com.unit4.zieloneludki.hackathon;


import android.graphics.Bitmap;
import android.os.Bundle;

import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by jacek on 03.02.18.
 */

public class SendPhotoDialog extends DialogFragment {
    private Bitmap bitmap;
    @BindView(R.id.iv_photo_taken)
    ImageView imageView;



    public SendPhotoDialog() {
    }

    static SendPhotoDialog newInstance(Bitmap bitmap) {
        SendPhotoDialog f = new SendPhotoDialog();
        f.setBitmap(bitmap);
        return f;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.photo_dialog, container, false);
        ButterKnife.bind(this, v);
        imageView.setImageBitmap(bitmap);
        return v;
    }


}

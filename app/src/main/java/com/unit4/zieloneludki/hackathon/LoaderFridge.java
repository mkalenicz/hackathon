package com.unit4.zieloneludki.hackathon;


import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;


//public class LoaderFridge extends AsyncTaskLoader<ArrayList<HashMap<String, String>>> {
public class LoaderFridge extends AsyncTaskLoader<ArrayList<HashMap<String, String>>> {

    public static final String API_URL = "https://fir-tutorial-7ec2e.firebaseio.com/fridge1/.json";
    //    public static final String API_URL = "https://matet-d79e1.firebaseio.com/.json";
//    public static final String API_URL = "https://matet-d79e1.firebaseio.com/matet/devices/0/.json";


    public static final int SUCCESS_CODE = 200;
    public boolean serverResponse;
    private ArrayList<HashMap<String, String>> fridgeList;
    private HashMap<String, String> fridgeElement;

    private String mUrl;

    public LoaderFridge(Context context, String url) {
        super(context);
        mUrl = url;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

    @Override
    public ArrayList<HashMap<String, String>> loadInBackground() {

        if (mUrl == null) {
            return null;
        }
        URL url = createUrl(API_URL);
        Log.i("NewsLoader", "URI_URL is: " + API_URL);
//        URL url = createUrl();
        String jsonResponse = "";
        try {
            jsonResponse = makeHttpRequest(url);
        } catch (IOException e) {
            Log.e("NewLoader", "Problem with loadInBackground method", e);
        }
        extractFromJson(jsonResponse);
        return fridgeList;
    }

    private String makeHttpRequest(URL url) throws IOException {
        String jsonResponse = "";
        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setReadTimeout(10000 /* milliseconds */);
            urlConnection.setConnectTimeout(15000 /* milliseconds */);
            urlConnection.connect();

            if (urlConnection.getResponseCode() == SUCCESS_CODE) {
                serverResponse = true;
                inputStream = urlConnection.getInputStream();
                jsonResponse = readFromStream(inputStream);
            } else {
                serverResponse = false;
                Log.e("NewsLoader", "Error response code" + urlConnection.getResponseCode());
            }
        } catch (IOException e) {
            Log.e("NewLoader", "Problem retrieving JSON results.", e);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (inputStream != null) {
                // function must handle java.io.IOException here
                inputStream.close();
            }
        }
        return jsonResponse;
    }

    private String readFromStream(InputStream inputStream) throws IOException {
        StringBuilder output = new StringBuilder();
        if (inputStream != null) {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
            BufferedReader reader = new BufferedReader(inputStreamReader);
            String line = reader.readLine();
            while (line != null) {
                output.append(line);
                line = reader.readLine();
            }
        }
        return output.toString();
    }

    private static URL createUrl(String stringUrl) {
        URL url = null;
        try {
            url = new URL(stringUrl);
        } catch (MalformedURLException exception) {
            Log.e("Error log", "Error with creating URL", exception);
            return null;
        }
        return url;
    }

    private ArrayList<HashMap<String, String>> extractFromJson(String deviceJSON) {

        fridgeList = new ArrayList<>();
        if (TextUtils.isEmpty(deviceJSON)) {
            return null;
        }
        try {
            JSONObject baseJsonResponse = new JSONObject(deviceJSON);
            JSONArray resultsArray = baseJsonResponse.getJSONArray("product");


            for (int i = 0; i < resultsArray.length(); i++) {

                JSONObject currentObject = resultsArray.getJSONObject(i);

                String attributeAmount = currentObject.getString("attributeAmount");
                String attributeDaysLeft = currentObject.getString("attributeDaysLeft");
                String attributeName = currentObject.getString("attributeName");

                fridgeElement = new HashMap<>();
                fridgeElement.put("attributeAmount", attributeAmount);
                fridgeElement.put("attributeDaysLeft", attributeDaysLeft);
                fridgeElement.put("attributeName", attributeName);

                fridgeList.add(fridgeElement);
            }

        } catch (JSONException e) {
            Log.e("Error LOG", "Problem parsing the JSON results", e);
        }
        return fridgeList;
    }

}
